create table usuario (cui varchar(13) PRIMARY KEY, nombre1 varchar(50) NOT NULL, apellido1 varchar(50) NOT NULL,correo varchar(50) NOT NULL, clave varchar(50) ); 
alter table usuario add rol int;
alter table usuario add CONSTRAINT FG_ROL_ROL FOREIGN KEY (rol) REFERENCES rol(rol);
create table menu(menu varchar(20) PRIMARY KEY,texto varchar(50) NOT NULL);
create table menu_rol(menu varchar(20) NOT NULL, rol int NOT NULL, PRIMARY KEY (menu,rol), CONSTRAINT FK_rol FOREIGN KEY (rol) REFERENCES rol(rol),CONSTRAINT FK_menu FOREIGN KEY (menu) REFERENCES menu(menu));

